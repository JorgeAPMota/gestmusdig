<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template match="/">
		<html>
		<head>
			<meta charset="utf-8"/>
			<title><h2>GestMusDig</h2></title>
		</head>	
			<body>
				<h1>My Music Collection, by music and gender:</h1>
				<table style="width:50%;" border="1">
					<tr bgcolor="#9acd32">
						<th style="text-align:center">Musica</th>
						<th style="text-align:center">Genero</th>
					</tr>
					<xsl:for-each select="GESTMusDig/musica">
					<tr>
						<td>
							<xsl:value-of select="nome"/>
						</td>
						<td>
							<xsl:value-of select="genero"/>
						</td>
					</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>