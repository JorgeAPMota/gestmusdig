<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

<xsl:template match="/">
    <xsl:element name="GESTMusDig">
		<xsl:element name="musica">
			<xsl:apply-templates select="GESTMusDig/musica"/>
		</xsl:element>
		<xsl:element name="interprete">
			<xsl:apply-templates select="GESTMusDig/interprete"/>
	  </xsl:element>
   </xsl:element>
</xsl:template>

<xsl:template match="GESTMusDig/musica">
   <xsl:element name="nome" >
      <xsl:value-of select="nome"/>
   </xsl:element>
   <xsl:element name="interprete" >
      <xsl:value-of select="faixa/interprete"/>
   </xsl:element>
</xsl:template>
<xsl:template match="GESTMusDig/interprete">
   <xsl:element name="nome" >
      <xsl:value-of select="nome"/>
   </xsl:element>
</xsl:template>

</xsl:stylesheet>